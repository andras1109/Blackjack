/*---------------------------------

               Config

---------------------------------*/

setLength = 5 // how many decks are in the set
minAmountToPlace = 0
maxAmountToPlace = 100
startingCredits = 500



/*----------------------------------

                Vars

----------------------------------*/
cards = []
credits = startingCredits
id = 0
typeList = ["C", "H", "D", "S"]
type = null
specialList = ["J", "Q", "K", "A"]
special = "n"
player = []
machine = []
numberOfPlayedRounds = 0
playersCardsValue = 0
machineCardsValue = 0
machineCardsNumber = 0
playersCardsNumber = 0
gO = ""

var UserWantsCard = document.getElementById("yes")
var UserDoesntWantsCard = document.getElementById("no")



/*--------------------------------

        Set creator

----------------------------------*/
for (let i = 0; i < setLength; i++) {
    id = id - id

    for (let j = 0; j < 4; j++) {
        id = id - id
        type = typeList[j]
        for (let k = 0; k < 13; k++) {
            special = "n"
            id = k + 2
            if (k == 13) {
                id = 11
                special = specialList[4]
            } else if (k >= 10) {
                id = 10
                special = specialList[k - 9]

            }
            cards[(i * 52) + (j * 13) + k - 65] = [type, id, special]
        }
    }
}



/*----------------------------------

        Card Printer

----------------------------------*/

function cardPrint(listToPrint) {

    if (listToPrint == player) {
        print("\n\nYour cards:")
    } else {
        print("\n\n\nDealer's cards:")
    }

    firstLine, secondLine, thirdLine, fourthLine, fifthLine, sixthLine = "", "", "", "", "", ""
    space = "   "
    for (let i = 0; i < listToPrint.length; i++) {
        if (listToPrint[i][3] == "n") {
            if (listToPRint[i][2] <= 9) {
                firstLine += " ______ " + space
                secondLine += "|" + toString(listToPrint[i][1]) + "     |" + space
                thirdLine += "|      |" + space
                fourthLine += "|   " + toString(listToPrint[i][2]) + "  |" + space
                fifthLine += "|      |" + space
                sixthLine += "|_____" + toString(listToPrint[i][1]) + "|" + space

            } else {
                firstLine += " ______ " + space
                secondLine += "|" + toString(listToPrint[i][1]) + "     |" + space
                thirdLine += "|      |" + space
                fourthLine += "|  " + toString(listToPrint[i][2]) + "  |" + space
                fifthLine += "|      |" + space
                sixthLine += "|_____" + toString(listToPrint[i][1]) + "|" + space

            }
        } else {
            firstLine += " ______ " + space
            secondLine += "|" + toString(listToPrint[i][1]) + space + toString(listToPrint[i][3]) + "|" + space
            thirdLine += "|      |" + space
            fourthLine += "|  " + toString(listToPrint[i][2]) + "  |" + space
            fifthLine += "|      |" + space
            sixthLine += "|" + toString(listToPrint[i][3]) + "____" + listToPrint[i][1] + "|" + space

        }
    }
    console.log(firstLine, " \n", secondLine, "\n", thirdLine, "\n", fourthLine, "\n", fifthLine, "\n", sixthLine, "\n")
}

/*----------------------------------

    Card Printer for hidden parts

----------------------------------*/

function inGameMachineCardsPrinter() {
    firstLine,
    secondLine,
    thirdLine,
    fourthLine,
    fifthLine,
    sixthLine = "",
    "",
    "",
    "",
    "",
    ""
    space = "   "

    print("\n\n\nDealer's cards:")
    for (let i = 0; i < machine.length; i++) {
        if (machine[i][3] == "n") {
            if (machine[i][2] <= 9) {
                firstLine += " ______ " + space
                secondLine += "|" + toString(machine[i][1]) + "     |" + space
                thirdLine += "|      |" + space
                fourthLine += "|   " + toString(machine[i][2]) + "  |" + space
                fifthLine += "|      |" + space
                sixthLine += "|_____" + toString(machine[i][1]) + "|" + space

            } else {
                firstLine += " ______ " + space
                secondLine += "|" + toString(machine[i][1]) + "     |" + space
                thirdLine += "|      |" + space
                fourthLine += "|  " + toString(machine[i][2]) + "  |" + space
                fifthLine += "|      |" + space
                sixthLine += "|_____" + toString(machine[i][1]) + "|" + space

            }
        } else {
            firstLine += " ______ " + space
            secondLine += "|" + toString(machine[i][1]) + space + toString(machine[i][3]) + "|" + space
            thirdLine += "|      |" + space
            fourthLine += "|  " + toString(machine[i][2]) + "  |" + space
            fifthLine += "|      |" + space
            sixthLine += "|" + toString(machine[i][3]) + "____" + machine[i][1] + "|" + space

        }
    }
    firstLine += " ______ " + space
    secondLine += "|" + "?" + space + "?" + "|" + space
    thirdLine += "|      |" + space
    fourthLine += "|  " + "?" + "  |" + space
    fifthLine += "|      |" + space
    sixthLine += "|" + "?" + "____" + "?" + "|" + space

    console.log(firstLine, " \n", secondLine, "\n", thirdLine, "\n", fourthLine, "\n", fifthLine, "\n", sixthLine, "\n")
}
/*----------------------------------

Game Over

----------------------------------*/
function gameOver(result) {
    console.log(result)
    player = []
    machine = []
    playersCardsValue = 0
    machineCardsValue = 0
    machineCardsNumber = 0
    playersCardsNumber = 0
    return
}



/*----------------------------------

                Turn

----------------------------------*/
function turn(memberToAdd, set) {
    machineCardsNumber = machine.length + 1
    playersCardsNumber = player.length

    numberOfPlayedRounds = numberOfPlayedRounds + machineCardsNumber + playersCardsNumber

    if (memberToAdd == machine) {
        machine[machineCardsNumber] = set[numberOfPlayedRounds + 4]
        machineCardsValue = machineCardsValue + set[numberOfPlayedRounds][2]
        machineCardsNumber = machineCardsNumber + 1
        return machine
    } else if (memberToAdd == player) {
        playersCardsNumber = playersCardsNumber + 1
        player[playersCardsNumber] = set[numberOfPlayedRounds + 4]
        playersCardsValue = playersCardsValue + player[playersCardsNumber][2]
    } else {
        print("valami nem jó xd")
    }
}

/*----------------------------------

            Dealer's turn

----------------------------------*/
function machineTurn(set) {
    while (machineCardsValue <= 16) {
        numberOfPlayedRounds = numberOfPlayedRounds + 1
        turn(machine, set)
    }


    if (machineCardsValue > 21) {
        gO = "dealer busted"
        cardPrint(player)
        cardPrint(machine)
        gameOver(gO)
    } else if (playersCardsValue == 21 && playersCardsValue != machineCardsValue) {
        gameOver("You won!")
        return
    } else if (machineCardsValue == playersCardsValue) {
        gO = "draw"
        cardPrint(player)
        cardPrint(machine)
        gameOver(gO)
    } else if (machineCardsValue > playersCardsValue) {
        gO = "dealer won"
        cardPrint(player)
        cardPrint(machine)
        gameOver(gO)
    }
}

/*----------------------------------

Player's rounds

----------------------------------*/
function wantCard(set) {
    turn(player, set)
    cardPrint(player)
    inGameMachineCardsPrinter()
    return
}

function yourTurn(set) {

    console.log("You Want another card?(y/n) ")
    UserWantsCard.onclick = function(){
        wannaCard = "y"
    }
    while (wannaCard != null) {

        UserWantsCard.onclick = function(){
            wannaCard = "y"
        }
        UserDoesntWantsCard.onclick = function(){
            wannaCard = "n"
        }
        if (playersCardsValue > 21) {
            gameOver("Dealer won!")
            return

        } else if (wannaCard == "y" && playersCardsValue < 21) {
            wantCard(set)
            wannaCard = ""

        } else if (wannaCard == "n") {
            then
            print("Dealer's turn!")
            machineTurn(set)
            return

        } else if (wannaCard == "") {
            then
            console.log("You Want another card?(y/n) ")
            wannaCard = string.lower(io.read())

        } else {
            console.log("Write 'y' or 'n' ")
            wannaCard = string.lower(io.read())
        }
    }
}
/*----------------------------------

The first Deal

----------------------------------*/
function firstDeal(set){
let pCard = []
let mCard = []

for (let i = 0; i < array.length; i++) {
    pCard = set[2 * i - 1 + numberOfPlayedRounds]
    playersCardsNumber = playersCardsNumber + 1
    mCard = set[2 * i + numberOfPlayedRounds]
    machineCardsNumber = machineCardsNumber + 1
    player[i] = pCard
    machine[i] = mCard
    playersCardsValue = playersCardsValue + pCard[2]
    machineCardsValue = machineCardsValue + mCard[2]
    end
    numberOfPlayedRounds = numberOfPlayedRounds + 4
    cardPrint(player)
    inGameMachineCardsPrinter()
    yourTurn(set)

}
}

/*
______
|Q    C|
|      |
|  10  |
|      |
|C____Q|

*/
/*----------------------------------

Shufflig the Set

----------------------------------*/
function setShuffler(set) {
    math.randomseed(os.time())
    for (let i = 0; i < set.length; i++) {
        rand = math.random(1, set.length)
        set[i], set[rand] = set[rand], set[i]
    }
    firstDeal(set)
    return set
}

/*----------------------------------

Placing bids

----------------------------------*/
function bid() {
    console.log("\nYour balance is: ", credits)
    console.log("\nPlace your bid: ")
    asd = +readline()
    while (typeof (asd) != "number") {
        console.log("that isn't a valid number.\nPlace your bid: ")
        asd = +readline()
    }
    while (asd > credits || asd > maxAmountToPlace) {
        console.log("that is more than you can place.\nPlace your bid: ")
        asd = tonumber(io.read())
        while (typeof (asd) != "number") {
            console.log("that isn't a valid number.\nPlace your bid: ")
            asd = tonumber(io.read())
        }
    }
}
bid()
/*--------------------------------

Infinite Rounds

----------------------------------*/
function infinite() {
    if (numberOfPlayedRounds <= setLength * 52 - 28) {
        firstDeal(cards)
    } else if (numberOfPlayedRounds > setLength * 52 - 28) {
        numberOfPlayedRounds = 0
        setShuffler(cards)
    } else {
        print("infinite()")
    }
}

function infinite2() {
    while (true) {
        console.log("wanna play 1 more? (y/n) ")
        jsjdjccjjrksox = string.lower(io.read())
        if (jsjdjccjjrksox == "y") {
            for (i = 0; 99; i++)
                print("\n")
            end
            infinite()
        } else if (jsjdjccjjrksox == "n") {

            print("Understandable have a great day")
            return 0
        } else {
            print("infinite2()")
            end
        }
    }
}
/*----------------------------------

Starting

----------------------------------*/
setShuffler(cards)
infinite2()