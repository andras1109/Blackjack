--[[-------------------------------

		Config

---------------------------------]]
setLength = 5 -- how many decks are in the set
minAmountToPlace = 0
maxAmountToPlace = 100
startingCredits = 500



--[[--------------------------------

		Vars

----------------------------------]]
cards = {}
id = 0
nameList = {"C", "H", "D", "S"}
name = nil
specialList = {"J", "Q", "K", "A"}
special = "n"
player = {}
machine = {}
numberOfPlayedRounds = 0
playersCardsValue = 0
machineCardsValue = 0
machineCardsNumber = 0
playersCardsNumber = 0
gO = ""



--[[--------------------------------

            Set creator

----------------------------------]]
for i = 1, setLength, 1 do
	id = id - id

	for j = 1, 4, 1 do
		id = id - id
		name = nameList[j]

		for k = 1, 13, 1 do
			special = "n"
			id = k + 1

			if k == 13 then
				id = 11
				special = specialList[4]
			elseif k >= 10 then
				id = 10
				special = specialList[k - 9]
			end
			cards[(i * 52) + (j * 13) + k - 65] = {name, id, special}
		end
	end
end



--[[--------------------------------

            Card Printer

----------------------------------]]
function cardPrint(listToPrint)
	if listToPrint == player then
		print("\n\nYour cards:")
	else
		print("\n\n\nDealer's cards:")
	end
	
	for i = 1, #listToPrint do
		if listToPrint[i][3] == "n" then
			if listToPrint[i][2] <= 9 then
				io.write(
					" ______\n|",
					listToPrint[i][1],
					"     |\n|      |\n|   ",
					listToPrint[i][2],
					"  |\n|      |\n|_____",
					listToPrint[i][1],
					"|\n"
				)
			else
				io.write(
					" ______\n|",
					listToPrint[i][1],
					"     |\n|      |\n|  ",
					listToPrint[i][2],
					"  |\n|      |\n|_____",
					listToPrint[i][1],
					"|\n"
				)
			end
		else
			io.write(
				" ______\n|",
				listToPrint[i][1],
				"    ",
				listToPrint[i][3],
				"|\n|      |\n|  ",
				listToPrint[i][2],
				"  |\n|      |\n|",
				listToPrint[i][3],
				"____",
				listToPrint[i][1],
				"|\n"
			)
		end
	end
end



--[[--------------------------------

    Card Printer for hidden parts

----------------------------------]]
function inGameMachineCardsPrinter()
	print("\n\n\nDealer's cards")
	for i = 1, #machine - 1 do
		if machine[i][3] == "n" then
			if machine[i][2] <= 9 then
				io.write(
					" ______\n|",
					machine[i][1],
					"     |\n|      |\n|   ",
					machine[i][2],
					"  |\n|      |\n|_____",
					machine[i][1],
					"|\n"
				)
			else
				io.write(
					" ______\n|",
					machine[i][1],
					"     |\n|      |\n|  ",
					machine[i][2],
					"  |\n|      |\n|_____",
					machine[i][1],
					"|\n"
				)
			end
		else
			io.write(
				" ______\n|",
				machine[i][1],
				"    ",
				machine[i][3],
				"|\n|      |\n|  ",
				machine[i][2],
				"  |\n|      |\n|",
				machine[i][3],
				"____",
				machine[i][1],
				"|\n"
			)
		end
	end
	io.write(" ______\n|", "?", "    ", "?", "|\n|      |\n|   ", "?", "  |\n|      |\n|", "?", "____?|\n")
end



--[[--------------------------------

            Game Over

----------------------------------]]
function gameOver(result)
	print(result)
	player = {}
	machine = {}
	playersCardsValue = 0
	machineCardsValue = 0
	machineCardsNumber = 0
	playersCardsNumber = 0
	return
end



--[[--------------------------------

		Turn

----------------------------------]]
function turn(memberToAdd, set)
	machineCardsNumber = #machine + 1
	playersCardsNumber = #player

	numberOfPlayedRounds = numberOfPlayedRounds + machineCardsNumber + playersCardsNumber

	if memberToAdd == machine then
		machine[machineCardsNumber] = set[numberOfPlayedRounds + 4]
		machineCardsValue = machineCardsValue + set[numberOfPlayedRounds][2]
		machineCardsNumber = machineCardsNumber + 1
		return machine
	elseif memberToAdd == player then
		playersCardsNumber = playersCardsNumber + 1
		player[playersCardsNumber] = set[numberOfPlayedRounds + 4]
		playersCardsValue = playersCardsValue + player[playersCardsNumber][2]
	else
		print("valami nem jó xd")
	end
end

--[[--------------------------------

            Dealer's turn

----------------------------------]]
function machineTurn(set)
	while machineCardsValue <= 16 do
		numberOfPlayedRounds = numberOfPlayedRounds + 1
		turn(machine, set)
	end

	if machineCardsValue > 21 then
		gO = "dealer busted"
		cardPrint(player)
		cardPrint(machine)
		gameOver(gO)
		
	elseif playersCardsValue == 21 and playersCardsValue ~= machineCardsValue then
		gameOver("You won!")
		return

	elseif machineCardsValue == playersCardsValue then
		gO = "draw"
		cardPrint(player)
		cardPrint(machine)
		gameOver(gO)
		
	elseif machineCardsValue > playersCardsValue then
		gO = "dealer won"
		cardPrint(player)
		cardPrint(machine)
		gameOver(gO)
		
	end
end

--[[--------------------------------

            Player's rounds

----------------------------------]]
function wantCard(set)
	turn(player, set)
	cardPrint(player)
	inGameMachineCardsPrinter()
	return
end

function yourTurn(set)
	
	io.write("You Want another card?(y/n) ")
	local wannaCard = string.lower(io.read())

	while wannaCard ~= nil do
		for i = 1, #player do
			if playersCardsValue > 21 and player[i][2] == 11 then
				player[i][2] = 1
			end
		end

	        if playersCardsValue > 21 then
        	        gameOver("Dealer won!")
		        return
	
		elseif wannaCard == "y" and playersCardsValue < 21 then
			wantCard(set)
			wannaCard = ""
	
			
		elseif wannaCard == "n" then
		        print("Dealer's turn!")
		        machineTurn(set)
	         	return
	         	
		elseif wannaCard == "" then
			io.write("You Want another card?(y/n) ")
			wannaCard = string.lower(io.read())
			
		else
			io.write("Write 'y' or 'n' ")
			wannaCard = string.lower(io.read())
			
		end
	end
end

--[[--------------------------------

            The first Deal

----------------------------------]]
function firstDeal(set)
	local pCard
	local mCard

	for i = 1, 2 do
		pCard = set[2 * i - 1 + numberOfPlayedRounds]
		playersCardsNumber = playersCardsNumber + 1
		mCard = set[2 * i + numberOfPlayedRounds]
		machineCardsNumber = machineCardsNumber + 1
		player[i] = pCard
		machine[i] = mCard
		playersCardsValue = playersCardsValue + pCard[2]
		machineCardsValue = machineCardsValue + mCard[2]
	end
	numberOfPlayedRounds = numberOfPlayedRounds + 4
	cardPrint(player)
	inGameMachineCardsPrinter()
	yourTurn(set)
end

--[[
 ______
|Q    C|
|      |
|  10  |
|      |
|C____Q|

]]
--[[--------------------------------

            Shufflig the Set

----------------------------------]]
function setShuffler(set)
	math.randomseed(os.time())

	for i = 1, #set, 1 do
		local temp = set[i]
		local rand = math.random(1, #set)
		set[i] = set[rand]
		set[rand] = temp
	end

	firstDeal(set)
	return set
end

--[[--------------------------------

            Infinite Rounds

----------------------------------]]
function infinite()
	if numberOfPlayedRounds <= setLength * 52 - 28 then
		firstDeal(cards)
	elseif numberOfPlayedRounds > setLength * 52 - 28 then
		numberOfPlayedRounds = 0
		setShuffler(cards)
	else
		print("infinite()")
	end
end
function infinite2()
	while true do
		io.write("wanna play 1 more? (y/n) ")
		local jsjdjccjjrksox = string.lower(io.read())
		if jsjdjccjjrksox == "y" then
			for i = 0, 99 do
				print("\n")
			end
			infinite()
		elseif jsjdjccjjrksox == "n" then
			print("Understandable have a great day")
			return 0
		else
			print("infinite2()")
		end
	end
end

--[[--------------------------------

            Starting

----------------------------------]]
setShuffler(cards)
infinite2()